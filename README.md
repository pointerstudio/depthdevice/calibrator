# calibrate depth devices

currently working with:
- Kinect Azure

approach:
- rough alignment via marker
- fine alignment via shape

# how to commit with compatible hashes of dependencies
git commit --all -m "commit message $(./compatibleHa.sh)"

# Kinect Azure issues
`replace_sample(). capturesync_drop, releasing capture early due to full queue TS`
frames cannot be processed fast enough somehow. There can be multiple issues:
## usb buffer too small
chance: very likely, especially if multiple cameras

read: https://www.flir.com/support-center/iis/machine-vision/application-note/understanding-usbfs-on-linux/

check usb memory size:
`$ cat /sys/module/usbcore/parameters/usbfs_memory_mb`

increase memory temporarily:
`$ sudo sh -c 'echo 2048 > /sys/module/usbcore/parameters/usbfs_memory_mb'` 

increase memory forever:

1. Open the /etc/default/grub file in any text editor. Find and replace:

`GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"`

with this:

`GRUB_CMDLINE_LINUX_DEFAULT="quiet splash usbcore.usbfs_memory_mb=2048"`

2. Update grub with these settings:

`$ sudo update-grub`

## usb bus too small or slow
chance: unlikely if up to 2 cameras, very likely if more than 3

fix: decrease resolution, use less cameras, buy usb extension card with individual usb busses

## gpu too slow
chance: no idea

fix: get a better gpu, decrase resolution, use less cameras
