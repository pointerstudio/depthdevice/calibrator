#!/bin/bash

entryDirectory=$(pwd)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if [ -n "$PG_OF_PATH" ]; then
    echo "detected oF-root $PG_OF_PATH"
else
    echo "did not find PG_OF_PATH"
    echo "what is your openframeworks path?"
    read -e -p "PG_OF_PATH:" PG_OF_PATH
fi

cd $PG_OF_PATH/addons

git clone --depth=1 git@gitlab.com:pointerstudio/utils/ofxCv

git clone --depth=1 git@gitlab.com:pointerstudio/utils/ofxPCL

git clone --depth=1 git@gitlab.com:pointerstudio/utils/ofxDepthDevice

git clone --depth=1 http://github.com/roymacdonald/ofxCameraSaveLoad

git clone --depth=1 http://github.com/nickhardeman/ofxGizmo

cd $entryDirectory

