#include "Parameters.h"

void Parameters::setup(ofJson json){
    gui.setup();

    stage.addListener(this,&Parameters::Listener);
}

void Parameters::update(){
}

void Parameters::draw(){
    auto mainSettings = ofxImGui::Settings();
    mainSettings.windowSize = glm::vec2(ofGetWidth() - 20,400);
    gui.begin();

    if(ofxImGui::BeginWindow("Settings", mainSettings, ImGuiWindowFlags_NoCollapse)){
        if (ofxImGui::BeginTree(general_parameters, mainSettings))
        {
            ofxImGui::AddRadio(stage, stageStrings, 4);
            if (ImGui::Button(next_stage.getName().c_str())) {
                next_stage.set(true);
            }

            ofxImGui::EndTree(mainSettings);
        }
        ofxImGui::EndWindow(mainSettings);
    }
    gui.end();
    mouseOverGui = mainSettings.mouseOverGui;
}

void Listener(ofAbstractParameter &e) {
}
