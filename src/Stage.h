#pragma once

#include <string>
#include <map>
#include <vector>

enum Stage {
    STAGE_CONNECT_CAMERAS,
    STAGE_ROUGH_CALIBRATE,
    STAGE_FINE_CALIBRATE,
    STAGE_EXPORT
};

const std::map <Stage, std::string> StageToString {
    {STAGE_CONNECT_CAMERAS, "STAGE_CONNECT_CAMERAS"},
    {STAGE_ROUGH_CALIBRATE, "STAGE_ROUGH_CALIBRATE"},
    {STAGE_FINE_CALIBRATE, "STAGE_FINE_CALIBRATE"},
    {STAGE_EXPORT, "STAGE_EXPORT"}
};

const std::vector <std::string> stageStrings {
    "STAGE_CONNECT_CAMERAS",
    "STAGE_ROUGH_CALIBRATE",
    "STAGE_FINE_CALIBRATE",
    "STAGE_EXPORT"
};
