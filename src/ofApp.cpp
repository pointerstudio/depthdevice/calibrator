#include "ofApp.h"
#include "ofUtils.h"

//--------------------------------------------------------------
void ofApp::setup(){

    parameters.setup();
    devicePool.setup();

    cam.enableMouseInput();
    cam.enableMouseMiddleButton();
}

//--------------------------------------------------------------
void ofApp::update(){
    parameters.update();
    devicePool.update();

    if (parameters.mouseOverGui) {
        cam.disableMouseInput();
    } else {
        cam.enableMouseInput();
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    cam.begin();
    devicePool.draw();
    cam.end();

    parameters.draw();
}

//--------------------------------------------------------------
void ofApp::exit(){
    devicePool.exit();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
