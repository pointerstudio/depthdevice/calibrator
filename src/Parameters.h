#pragma once

#include "ofMain.h"
#include "ofxImGui.h"

#include "Stage.h"

class Parameters {

    public:
        void setup(ofJson json=NULL);
        void update();
        void draw();

        void Listener(ofAbstractParameter & e);

        bool mouseOverGui{false};

        ofParameter <int> stage{"Stage", STAGE_CONNECT_CAMERAS};
        ofParameter <bool> next_stage{"Next", false};
        ofParameter <bool> previous_stage{"Previous", false};
        ofParameter <string> export_filename{"Export Filename",""};
        ofParameterGroup general_parameters{
            "general", stage, previous_stage, next_stage
        };

        unordered_multimap <Stage, ofParameterGroup> stage_parameters{
            {STAGE_CONNECT_CAMERAS,
             ofParameterGroup{"connected_cameras"}}
        };

    protected:

        ofxImGui::Gui gui;
};
