#version 460

// OF
uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 modelMatrix;
uniform mat4 normalMatrix;
in vec4 position;
in vec4 color;
in vec4 normal;
in vec2 texcoord;

// MINE
uniform float time;
uniform float normalMix;

out vec4 v_color;

float map(float value, float inMin, float inMax, float outMin, float outMax) {
  return outMin + (outMax - outMin) * (value - inMin) / (inMax - inMin);
}

vec2 map(vec2 value, vec2 inMin, vec2 inMax, vec2 outMin, vec2 outMax) {
  return outMin + (outMax - outMin) * (value - inMin) / (inMax - inMin);
}

vec3 map(vec3 value, vec3 inMin, vec3 inMax, vec3 outMin, vec3 outMax) {
  return outMin + (outMax - outMin) * (value - inMin) / (inMax - inMin);
}

vec4 map(vec4 value, vec4 inMin, vec4 inMax, vec4 outMin, vec4 outMax) {
  return outMin + (outMax - outMin) * (value - inMin) / (inMax - inMin);
}


float myRandomMagic(vec3 p)  // replace this by something better
{
    p  = 50.0*fract( p*0.3183099 + vec3(0.71,0.113,0.419));
    return -1.0+2.0*fract( p.x*p.y*p.z*(p.x+p.y+p.z) );
}



// returns 3D value noise and its 3 derivatives
 vec4 noised( in vec3 x )
 {
    vec3 p = floor(x);
    vec3 w = fract(x);

    vec3 u = w*w*w*(w*(w*6.0-15.0)+10.0);
    vec3 du = 30.0*w*w*(w*(w-2.0)+1.0);

    float a = myRandomMagic( p+vec3(0,0,0) );
    float b = myRandomMagic( p+vec3(1,0,0) );
    float c = myRandomMagic( p+vec3(0,1,0) );
    float d = myRandomMagic( p+vec3(1,1,0) );
    float e = myRandomMagic( p+vec3(0,0,1) );
    float f = myRandomMagic( p+vec3(1,0,1) );
    float g = myRandomMagic( p+vec3(0,1,1) );
    float h = myRandomMagic( p+vec3(1,1,1) );

    float k0 =   a;
    float k1 =   b - a;
    float k2 =   c - a;
    float k3 =   e - a;
    float k4 =   a - b - c + d;
    float k5 =   a - c - e + g;
    float k6 =   a - b - e + f;
    float k7 = - a + b + c - d + e - f - g + h;

    return vec4( -1.0+2.0*(k0 + k1*u.x + k2*u.y + k3*u.z + k4*u.x*u.y + k5*u.y*u.z + k6*u.z*u.x + k7*u.x*u.y*u.z),
                 2.0* du * vec3( k1 + k4*u.y + k6*u.z + k7*u.y*u.z,
                                 k2 + k5*u.z + k4*u.x + k7*u.z*u.x,
                                 k3 + k6*u.x + k5*u.y + k7*u.x*u.y ) );
}
const mat3 m3  = mat3( 0.00,  0.80,  0.60,
                      -0.80,  0.36, -0.48,
                      -0.60, -0.48,  0.64 );
const mat3 m3i = mat3( 0.00, -0.80, -0.60,
                       0.80,  0.36, -0.48,
                       0.60, -0.48,  0.64 );
const mat2 m2 = mat2(  0.80,  0.60,
                      -0.60,  0.80 );
const mat2 m2i = mat2( 0.80, -0.60,
                       0.60,  0.80 );

// returns 3D fbm and its 3 derivatives
vec4 fbm( in vec3 x, int octaves )
{
    float f = 1.98;  // could be 2.0
    float s = 0.49;  // could be 0.5
    float a = 0.0;
    float b = 0.5;
    vec3  d = vec3(0.0);
    mat3  m = mat3(1.0,0.0,0.0,
    0.0,1.0,0.0,
    0.0,0.0,1.0);
    for( int i=0; i < octaves; i++ )
    {
        vec4 n = noised(x);
        a += b*n.x;          // accumulate values
        d += b*m*n.yzw;      // accumulate derivatives
        b *= s;
        x = f*m3*x;
        m = f*m3i*m;
    }
    return vec4( a, d );
}

void main(){

    vec4 pos =  modelViewProjectionMatrix * position;

    vec3 lightPos = vec3(sin(time) * 100,0,0);
    vec3 viewPos = vec3(0,time,0);
    vec3 lightDir   = normalize(lightPos - position.xyz);
    vec3 viewDir    = normalize(viewPos - position.xyz);
    vec3 halfwayDir = normalize(lightDir + viewDir);

    float spec = pow(max(dot(normal.xyz, halfwayDir), 0.0), 1.0);

    vec4 norm = normalize(transpose(inverse(mat4(modelViewMatrix))) * normal);
    float dotter = dot(normal.xyz, normalize(lightDir));
    float power = pow(dotter, 5.0);
    
    vec4 c = vec4(color.rgb * map(smoothstep(0.0,0.3,norm.g),0,1,0.1,1),1);//vec4(0,1,0,1);

    float mixer = 0;// max(0.0,sin(position.x * 0.001 + time * 4.2)) * 1;// * 0.5 + 0.5;
    vec3 almostNormal = (vec3(1,1,1) - color.rgb) * vec3(1,1,1);//normal.xyz * 0.5 + vec3(0.5);
    int octave = int((sin(time * 42) * 0.5 + 0.5) * 42) +  2;
    gl_Position = mix(pos,pos + (100*fbm(position.xyz * 0.01,octave)) - vec4(-100), mixer);
    v_color = vec4(
            mix(color.rgb,almostNormal,mixer)
            //vec3(mixer)
            ,1);//max(c,c2);
}

